package StationaryShop;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;

public class Stationary03 {
    public static void main(String[] args) {
        
        List<StationaryShopDetails> stationaryTable = Arrays.asList(
            new StationaryShopDetails("9/1/14", "Central", "Smith", 23001, "Desk", 2, 125.00, 250.00),
            new StationaryShopDetails("6/17/15","Central", "Kivell", 23001, "Desk", 5, 125.00, 625.00),
            new StationaryShopDetails("9/10/15", "Central", "Gill", 23002, "Pencil", 7, 1.29, 9.03),
            new StationaryShopDetails("11/17/15", "Central", "Jardine", 23003, "Binder", 11, 4.99, 54.89), 
            new StationaryShopDetails("10/31/15", "Central", "Andrews", 23002, "Pencil", 14, 1.29, 18.06), 
            new StationaryShopDetails("2/26/14","Central", "Gill", 23004, "Pen", 27, 19.99, 539.73), 
            new StationaryShopDetails("10/5/14", "Central", "Morgan", 23003, "Binder", 28, 8.99, 251.72), 
            new StationaryShopDetails("12/21/15", "Central", "Andrews",	23003, "Binder", 28, 4.99, 139.72), 
            new StationaryShopDetails("2/9/14",	"Central", "Jardine", 23002, "Pencil", 36, 4.99, 179.64), 
            new StationaryShopDetails("8/7/15",	"Central", "Kivell", 23005, "Pen Set", 42, 23.95, 1005.90), 
            new StationaryShopDetails("1/15/15", "Central", "Gill", 23003, "Binder", 46, 8.99, 413.54), 
            new StationaryShopDetails("1/23/14", "Central", "Kivell", 23003, "Binder", 50, 19.99, 999.50), 
            new StationaryShopDetails("3/24/15", "Central", "Jardine", 23005, "Pen Set", 50, 4.99, 249.50), 
            new StationaryShopDetails("5/14/15", "Central", "Gill", 23002, "Pencil", 53, 1.29, 68.37));
    
    /*Syntax : IntStream mapToInt(ToIntFunction<? super T> mapper)
    mapToInt() method belongs to stream interface. This method return an “IntStream” which consists the output of applying the provided function to the elements of its stream.
    ToIntFunction mapper is an intermediate operation.
 */
    OptionalInt min = stationaryTable.stream().mapToInt(StationaryShopDetails::getNumberofUnits).min();
    if (min.isPresent()) {
      System.out.printf("Lowest number of units has bought is %d\n", min.getAsInt());
    } else {
      System.out.println("min failed");
    }    
    }
}
