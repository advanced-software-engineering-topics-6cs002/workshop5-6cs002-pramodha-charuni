/* 
 StationaryShopDetails class includes the data of the product selling details of a stationary shop.
In this class, StationaryShopDetails class implements comparable interface. 
In Java comparable interface we can order user-defined class’s objects. 
Comparable interface can found  in java.lang package. From using “compareTo(Object)” method, 
We can compare the current object with the specified object. 
 */

package StationaryShop;

public class StationaryShopDetails implements Comparable<StationaryShopDetails> {

    private String orderDate;
    private String region;
    private String customerName;
    private int itemCode;
    private String itemName;
    private int numberofUnits;
    private double unitCost;
    private double totalAmount;

    public StationaryShopDetails(String orderDate, String region, 
    String customerName, int itemCode, String itemName, int numberofUnits,
    double unitCost, double totalAmount){

        this.orderDate = orderDate;
        this.region = region;
        this.customerName = customerName;
        this.itemCode = itemCode;
        this.itemName = itemName;
        this.numberofUnits = numberofUnits;
        this.unitCost = unitCost;
        this.totalAmount = totalAmount;

    }   
    
    @Override
    public String toString() {
        return "StationaryShopDetails [orderDate=" + orderDate + ", region=" + region + ", customerName=" + customerName
                + ", itemCode=" + itemCode + ", itemName=" + itemName + ", numberofUnits=" + numberofUnits
                + ", unitCost=" + unitCost + ", totalAmount=" + totalAmount + "]";
    }



    public String getOrderDate() {
        return orderDate;
    }


    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }


    public String getRegion() {
        return region;
    }


    public void setRegion(String region) {
        this.region = region;
    }


    public String getCustomerName() {
        return customerName;
    }


    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }


    public int getItemCode() {
        return itemCode;
    }


    public void setItemCode(int itemCode) {
        this.itemCode = itemCode;
    }


    public String getItemName() {
        return itemName;
    }


    public void setItemName(String itemName) {
        this.itemName = itemName;
    }


    public int getNumberofUnits() {
        return numberofUnits;
    }


    public void setNumberofUnits(int numberofUnits) {
        this.numberofUnits = numberofUnits;
    }


    public double getUnitCost() {
        return unitCost;
    }


    public void setUnitCost(double unitCost) {
        this.unitCost = unitCost;
    }


    public double getTotalAmount() {
        return totalAmount;
    }


    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }


    @Override
    public int compareTo(StationaryShopDetails SHD) {
        
        return ((Integer) itemCode).compareTo(SHD.itemCode);
      
    }

    
}
