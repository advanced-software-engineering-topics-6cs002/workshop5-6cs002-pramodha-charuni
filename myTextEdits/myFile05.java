package myTextEdits;

import java.io.BufferedReader;
import java.io.FileReader;

/* In this set of codes, we can arrange the lines of the cinderella.txt files,
 * using sort(), So the long lines are printing first and short lines are printing at last.
 */
public class myFile05 {
    public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("data/cinderella.txt"));

    r.lines().sorted((a, b) -> {
      if (a.length() == b.length())
        return 0;
      if (a.length() < b.length())
        return 1;
      return -1;
    }).forEach(l -> System.out.println(l));

    r.close();
  }
}
