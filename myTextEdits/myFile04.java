package myTextEdits;

import java.io.BufferedReader;
import java.io.FileReader;

/*
 * In this set of codes we can print the content of cinderella.txt file, in uppercase.
 */
public class myFile04 {
    public static void main(String[] args) throws Exception {
    BufferedReader r  = 
        new BufferedReader(new FileReader("data/cinderella.txt"));

    r.lines()
     .map(l -> l.toUpperCase())
     .forEach(l -> System.out.println(l));
    
    r.close();
}

}