package myTextEdits;

import java.io.BufferedReader;
import java.io.FileReader;

/*
 * In this set of code, we can print the cinderella.txt file's all lines as one paragraph.
 * reduce() demonstrate a reduce operation.
 * It uses different synax to access the concat method of the String class.
 */
public class myFile06 {
    public static void main(String[] args) throws Exception {
    BufferedReader r  = 
       new BufferedReader(new FileReader("data/cinderella.txt"));

    System.out.println(r.lines().reduce("", String::concat));
    
    r.close();
  }
}
