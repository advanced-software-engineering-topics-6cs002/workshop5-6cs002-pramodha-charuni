package myTextEdits;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import java.util.stream.Collectors;

/* 
Stream.collect() Method allows us to perform mutable fold operations on data elements held in a Stream instance. 
In this set of code, lines of the file in cinderella.txt, 
combining it's text into a list using collector. */

public class myFile08 {
     public static void main(String[] args) throws Exception {
    BufferedReader r  = 
        new BufferedReader(new FileReader("data/cinderella.txt"));

    List<String> l = r.lines().collect(Collectors.toList());
    
    for(String line: l){
      System.out.println(line);
    }
    
    r.close();
  }
}
