package myTextEdits;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Optional;

/*
 * Optional Type in Java is used to represent a value that may or may not be present.
 * An Optional object can either contain a non-null value or it can contain no value at all 
 */

public class myFile07 {
    public static void main(String[] args) throws Exception {
    BufferedReader r  = 
      new BufferedReader(new FileReader("data/cinderella.txt"));

    Optional <String >result = 
      r.lines()
       .reduce((left, right) -> left.concat(" ".concat(right)));
    
    if(result.isPresent())
      System.out.println("result is : " + result.get());
    else
      System.out.println("result not present : ");
    r.close();
}

}