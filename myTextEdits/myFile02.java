package myTextEdits;

import java.io.BufferedReader;
import java.io.FileReader;

/*
 * In this set of code we can count the number of lines in the cinderella.txt file.
 */
public class myFile02 {
    public static void main(String[] args) throws Exception {
    BufferedReader r  = 
      new BufferedReader(new FileReader("data/cinderella.txt"));

    System.out.println(r.lines().count());

    r.close();
  }
}
