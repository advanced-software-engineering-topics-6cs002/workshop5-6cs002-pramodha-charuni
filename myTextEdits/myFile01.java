package myTextEdits;

import java.io.BufferedReader;
import java.io.FileReader;


/* I have selected a story and put into a .txt file named cinderella.txt
 * Now, from the below code, I can run that story; All lines
 */
public class myFile01 {
    public static void main(String[] args) throws Exception {

    BufferedReader r = new BufferedReader(new FileReader("data/cinderella.txt"));

    r.lines().forEach(l -> System.out.println(l));

    r.close();
  }
}
