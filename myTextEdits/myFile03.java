package myTextEdits;

import java.io.BufferedReader;
import java.io.FileReader;

/*
 * In this set of codes we can filter the lines which contains the word "her",
 * Using the contains() method
 */
public class myFile03 {
     public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("data/cinderella.txt"));

    r.lines().filter(l -> l.contains("her"))
        .forEach(l -> System.out.println(l));

    r.close();
  }
}
